/*
		OBS. en del variabler kan beh�va �ndras i denna fil f�r korrekt beteende
		Uppdatera Offsets filerna vid en csgo uppdate
		csgo m�ste k�ras i fullscreen windowed/borderless/windowed f�r att overlay-f�nstret skall synas
*/

#include <windows.h>
#include "d3d9.h"
#include <iostream>
#include "d3dx9.h"
#include <Dwmapi.h> 
#include <TlHelp32.h>  
#include "PMemory.h"
#include "Game.h"
#include <Windows.h>
#include <string>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <thread>
#include <chrono>
#include <random>
#include <sstream>
// CREDITS: Seras
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "Dwmapi.lib")



//CSGO-relaterade globals:
PModule modClient;
PModule modEngine;
bool readisInited = 0;
PMemory mem;
int deltaX = 0;		//Hur stor recoilen �r i x-led
int deltaY = 0; 
int inAccuracy = 0;	//Anv�nds ej �n, men ska bli ett m�tt p� hur snett skotten kommer g� 
bool RCSstate = 0;	//Om den ska (f�rs�ka) dra siktet �t dig
Player me;
PlayerEntity nr[64];
DWORD dwPlayer;
DWORD dwEngine;
int tickcount;
int synced = 1;
int Flags;
float currSens = 1.0;


//�vriga globals:
#define PI 3.14159265
POINT cursor;		//En struct med int x och y, anv�nds f�r GetCursorPos



//Deklerationer:
void RCSmain();	//Har koll p� RCS och RCS-dot
void readis();	//L�ser minnet f�r csgo variabler
void syncer();	//Syncar �vriga funktioner s� de endast k�rs en g�ng per tick.
void bhopper();	//Bhoppar �t en om space h�lls nedtryckt!


//GUI/overlay relaterade globals:



int Width = 800;	//Standard v�rde f�r bredden p� f�nstret innan det s�tts med CSGO's storlek
int Height = 600;


const MARGINS Margin = { 0, 0, Width, Height };


char strTest[128] = "Kebab";

char lWindowName[256] = "SerOverlay";
HWND hWnd;

char tWindowName[256] = "Counter-Strike: Global Offensive"; /* tWindowName ? Target Window Name */
HWND tWnd;
RECT tSize;

MSG Message;
IDirect3D9Ex* p_Object = 0;
IDirect3DDevice9Ex* p_Device = 0;
D3DPRESENT_PARAMETERS p_Params;

ID3DXLine* p_Line;
ID3DXFont* pFontSmall = 0;

int toPixels(float conversionAngle) {
	int pixelsPerDegree = Width / 90;
	return int(((conversionAngle / (1.6 /*1.6 b�r vara ens mousesens i csgo, annars hade jag tur n�r jag gissa*/ * 2)) * pixelsPerDegree) + 0.5 /*0.5 �r f�r att avrunda �t r�tt h�ll*/);
}

int DirectXInit(HWND hWnd)
{
	if (FAILED(Direct3DCreate9Ex(D3D_SDK_VERSION, &p_Object)))
		exit(1);

	ZeroMemory(&p_Params, sizeof(p_Params));
	p_Params.Windowed = TRUE;
	p_Params.SwapEffect = D3DSWAPEFFECT_DISCARD;
	p_Params.hDeviceWindow = hWnd;
	p_Params.MultiSampleQuality = D3DMULTISAMPLE_NONE;
	p_Params.BackBufferFormat = D3DFMT_A8R8G8B8; //B�r ej �ndras d� mkt skulle breakas
	p_Params.BackBufferWidth = Width;
	p_Params.BackBufferHeight = Height;
	p_Params.EnableAutoDepthStencil = TRUE;
	p_Params.AutoDepthStencilFormat = D3DFMT_D16;

	if (FAILED(p_Object->CreateDeviceEx(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &p_Params, 0, &p_Device)))
		exit(1);

	if (!p_Line)
		D3DXCreateLine(p_Device, &p_Line);
	p_Line->SetAntialias(1); //*removed cuz crosshair was blurred*

	D3DXCreateFont(p_Device, 18, 0, 0, 0, false, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, "Calibri", &pFontSmall);

	return 0;
}

int DrawString(char* String, int x, int y, int r, int g, int b, ID3DXFont* ifont)	//L�gg till en Alpha kanal f�r hur genomskinlig den ska vara
{
	RECT ShadowPos;
	ShadowPos.left = x + 1;
	ShadowPos.top = y + 1;
	RECT FontPos;
	FontPos.left = x;
	FontPos.top = y;
	ifont->DrawTextA(0, String, strlen(String), &ShadowPos, DT_NOCLIP, D3DCOLOR_ARGB(255, r / 3, g / 3, b / 3));
	ifont->DrawTextA(0, String, strlen(String), &FontPos, DT_NOCLIP, D3DCOLOR_ARGB(255, r, g, b));
	return 0;
}

int DrawRealString(std::string text, int x, int y, int r, int g, int b, int a, ID3DXFont* ifont)	//L�gg till en Alpha kanal f�r hur genomskinlig den ska vara
{
	char * temp = new char[text.size() + 1];
	std::strcpy(temp, text.c_str());
	RECT ShadowPos;
	ShadowPos.left = x + 1;
	ShadowPos.top = y + 1;
	RECT FontPos;
	FontPos.left = x;
	FontPos.top = y;
	ifont->DrawTextA(0, temp, strlen(temp), &ShadowPos, DT_NOCLIP, D3DCOLOR_ARGB(a, r / 3, g / 3, b / 3));
	ifont->DrawTextA(0, temp, strlen(temp), &FontPos, DT_NOCLIP, D3DCOLOR_ARGB(a, r, g, b));
	return 0;
}

void DrawLine(float x, float y, float xx, float yy, int r, int g, int b, int a)	//Ritar en linje mellan xy och xxyy med redgreenbluealpha kod
{
	D3DXVECTOR2 dLine[2];

	p_Line->SetWidth(1);

	dLine[0].x = x;
	dLine[0].y = y;

	dLine[1].x = xx;
	dLine[1].y = yy;

	p_Line->Draw(dLine, 2, D3DCOLOR_ARGB(a, r, g, b));

}
void FillRGB(float x, float y, float w, float h, int r, int g, int b, int a)	//Ev. l�gga till centered flagga som argument s� att x,y �r i mitten ist f�r topleft
{
	D3DXVECTOR2 vLine[2];

	p_Line->SetWidth(w);

	vLine[0].x = x + w / 2;
	vLine[0].y = y;
	vLine[1].x = x + w / 2;
	vLine[1].y = y + h;

	p_Line->Begin();
	p_Line->Draw(vLine, 2, D3DCOLOR_RGBA(r, g, b, a));
	p_Line->End();
}

void DrawCircle(int X, int Y, int radius, int numSides, int width, int r, int g, int b, int a)	//numSides kan s�ttas f�r �nskad effekt (b�r bli triangel med 3 och 4-kant med 4 osv.)
{
	D3DXVECTOR2 Line[128];
	float Step = PI * 2.0 / numSides;
	int Count = 0;
	for (float a = 0; a < PI*2.0; a += Step)
	{
		float X1 = radius * cos(a) + X;
		float Y1 = radius * sin(a) + Y;
		float X2 = radius * cos(a + Step) + X;
		float Y2 = radius * sin(a + Step) + Y;
		Line[Count].x = X1;
		Line[Count].y = Y1;
		Line[Count + 1].x = X2;
		Line[Count + 1].y = Y2;
		Count += 2;
	}
	p_Line->SetWidth(width);
	p_Line->Begin();
	p_Line->Draw(Line, Count, D3DCOLOR_RGBA(r, g, b, a));
	p_Line->End();
}

void DrawBox(float x, float y, float width, float height, float px, int r, int g, int b, int a)
{
	D3DXVECTOR2 points[5];
	points[0] = D3DXVECTOR2(x, y);
	points[1] = D3DXVECTOR2(x + width, y);
	points[2] = D3DXVECTOR2(x + width, y + height);
	points[3] = D3DXVECTOR2(x, y + height);
	points[4] = D3DXVECTOR2(x, y);
	p_Line->SetWidth(1); //Kan vara vettig att sl�nga upp som argument
	p_Line->Draw(points, 5, D3DCOLOR_RGBA(r, g, b, a));
}

int Render() //Det �r denna som �r huvudloopen f�r vad som kommer renderas, l�gg in skit i if-satsen
{
	p_Device->Clear(0, 0, D3DCLEAR_TARGET, 0, 1.0f, 0);
	p_Device->BeginScene();

	if (tWnd == GetForegroundWindow())
	{
		DrawString("Nj�pp gjort av Seras", 5, 5, 240, 240, 250, pFontSmall);
		strcpy_s(strTest , std::to_string(tickcount).c_str());
		char tickcountStr[128] = "Tickcount: ";
		strcat_s(tickcountStr, strTest);
		DrawString(tickcountStr, 5, 25, 240, 240, 250, pFontSmall);
		



		if (GetAsyncKeyState(VK_TAB)) {
			int TeamCountCT = 0;
			int TeamCountT = 0;
			int TeamHealthCountCT = 0;
			int TeamHealthCountT = 0;
			int x = 0;
			int y = 0;
			for ( int i = 0 ; i < 64 ; i++ ) {
				if (nr[i].Team == 3) {
					TeamCountCT++;
					if (nr[i].Health < 101 && 0 < nr[i].Health) {
						TeamHealthCountCT += nr[i].Health;
						DrawRealString(std::to_string(nr[i].Health), 5, (505 + (20 * x++)), 151, 185, 239, 255, pFontSmall);
					}
				}
				else if (nr[i].Team == 2) {
					TeamCountT++;
					if (nr[i].Health < 101 && 0 < nr[i].Health) {
						TeamHealthCountT += nr[i].Health;
						DrawRealString(std::to_string(nr[i].Health), 85, (505 + (20 * y++)), 255, 226, 149, 255, pFontSmall);
					}
				}
				
				
			}
			DrawRealString(std::to_string(TeamCountT), 5, 425, 240, 240, 40, 255, pFontSmall);
			DrawRealString(std::to_string(TeamCountCT), 5, 445, 40, 40, 240, 255, pFontSmall);
			DrawRealString(std::to_string(TeamHealthCountT), 5, 465, 240, 240, 40, 255, pFontSmall);
			DrawRealString(std::to_string(TeamHealthCountCT), 5, 485, 40, 40, 240, 255, pFontSmall);

		}
		else {
			if (RCSstate == 1) {
				DrawString("RCS: Enabled!", 5, 45, 240, 240, 250, pFontSmall);
			}
			else {
				DrawString("RCS: Disabled!", 5, 45, 240, 240, 250, pFontSmall);
			}

			if ((Flags & 0x1) == 1) {
				DrawString("Can JUMP!", 5, 65, 240, 240, 250, pFontSmall);
			}
			/*char FJ1[128];
			strcpy_s(FJ1, std::to_string(Offsets()->dwForceJump).c_str());
			DrawString(FJ1, 5, 85, 240, 0, 0, pFontSmall);*/	//Printar ut addressen f�r ForceJump i talbas 10
			/*char MS[128];
			strcpy_s(MS, std::to_string(currSens).c_str());
			DrawString(MS, 5, 85, 240, 0, 0, pFontSmall);*/		//Printar ut nuvarande sens p� musen ingame

			FillRGB((Width / 2) - (1 + deltaY), (Height / 2) - (1 - deltaX), 3, 3, 255, 0, 0, 255);
			//FillRGB(5, 30, 10, 10, 0, 255, 0, 255);
			//FillRGB(5, 40, 10, 10, 0, 0, 255, 255);

			DrawCircle(((Width / 2) - deltaY), ((Height / 2) + deltaX), 15, 8, 2, 255, 0, 255, 255);

			//DrawBox(5, 50, 20, 20, 1, 255, 0, 0, 255);
			//DrawLine(5, 60, 80, 91, 24, 24, 24, 255);
		}

	}

	p_Device->EndScene();
	p_Device->PresentEx(0, 0, 0, 0, 0);
	if (GetAsyncKeyState(VK_END) & 0x8000) {
		Sleep(250);
		terminate();
	}
	return 0;
}

LRESULT CALLBACK WinProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	switch (Message)
	{
	case WM_PAINT:
		Render();
		break;

	case WM_CREATE:
		DwmExtendFrameIntoClientArea(hWnd, &Margin);
		break;

	case WM_DESTROY:
		PostQuitMessage(1);
		return 0;

	default:
		return DefWindowProc(hWnd, Message, wParam, lParam);
		break;
	}
	return 0;
}

void SetWindowToTarget()
{
	while (true)
	{
		tWnd = FindWindow(0, tWindowName);
		if (tWnd)
		{
			GetWindowRect(tWnd, &tSize);
			Width = tSize.right - tSize.left; //H�r s�tts overlay-f�nstret till tWindowName's storlek (csgo)
			Height = tSize.bottom - tSize.top;
			DWORD dwStyle = GetWindowLong(tWnd, GWL_STYLE);
			if (dwStyle & WS_BORDER) //skall kompensera om borderless ej k�rs
			{
				tSize.top += 23;
				Height -= 23;
			}
			MoveWindow(hWnd, tSize.left, tSize.top, Width, Height, true);
		}
		else
		{
			char ErrorMsg[125];
			sprintf_s(ErrorMsg, "Make sure %s is running!", tWindowName);
			MessageBox(0, ErrorMsg, "Error - Cannot find the game!", MB_OK | MB_ICONERROR);
			exit(1);
		}
		Sleep(100);
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hSecInstance, LPSTR nCmdLine, INT nCmdShow) //Detta �r main funktionen som skall k�ras f�r WINDOWS OS
{
	srand(time(NULL));


	//Tr�dar skapas p� detta vis h�r
	std::thread t1(readis);
	std::thread t2(syncer);
	std::thread t3(RCSmain);
	std::thread t4(bhopper);
	//std::thread t2(funktionsnamn p� loopen som ska k�ras);
	CreateThread(0, 0, (LPTHREAD_START_ROUTINE)SetWindowToTarget, 0, 0, 0);
	


	WNDCLASSEX wClass;
	wClass.cbClsExtra = NULL;
	wClass.cbSize = sizeof(WNDCLASSEX);
	wClass.cbWndExtra = NULL;
	wClass.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(0, 0, 0));
	wClass.hCursor = LoadCursor(0, IDC_ARROW);
	wClass.hIcon = LoadIcon(0, IDI_APPLICATION);
	wClass.hIconSm = LoadIcon(0, IDI_APPLICATION);
	wClass.hInstance = hInstance;
	wClass.lpfnWndProc = WinProc;
	wClass.lpszClassName = lWindowName;
	wClass.lpszMenuName = lWindowName;
	wClass.style = CS_VREDRAW | CS_HREDRAW;

	if (!RegisterClassEx(&wClass))
		exit(1);

	tWnd = FindWindow(0, tWindowName); //hittar f�nstret med det namn som getts i tWindowName
	if (tWnd)	//Om den lyckats k�r
	{
		GetWindowRect(tWnd, &tSize);	
		Width = tSize.right - tSize.left;
		Height = tSize.bottom - tSize.top;
		hWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TRANSPARENT | WS_EX_LAYERED, lWindowName, lWindowName, WS_POPUP, 1, 1, Width, Height, 0, 0, 0, 0); //H�r skapas f�nstret med valda parametrar topmost k�r att det syns �ver csgo (f�r det mesta) transparent g�r att klick g�r igenom layered g�r att man ser igenom (tror jag)
		SetLayeredWindowAttributes(hWnd, 0, 1.0f, LWA_ALPHA);
		SetLayeredWindowAttributes(hWnd, 0, RGB(0, 0, 0), LWA_COLORKEY);
		ShowWindow(hWnd, SW_SHOW);
	}

	DirectXInit(hWnd);	//initierar d3dx9 mm.

	for (;;)
	{
		if (PeekMessage(&Message, hWnd, 0, 0, PM_REMOVE))
		{
			DispatchMessage(&Message);
			TranslateMessage(&Message);
		}
		Sleep(1);
	}
	return 0;
}






int whichKeyIsPressed() { //Programmet sitter fast i denna loop tills en knapp nedtrycks, dess Vkey-kod returneras
	while (true) {
		for (int i = 1; i < 255; i++) {
			if (GetAsyncKeyState(i) & 0x8000) {
				return i;
			}
		}
		Sleep(10);
	}
}




void RCSmain() {
	int toggleKey;
	bool activated = false; //Standard start v�rde, om whichKeyIsPressed anv�nds kommer den hinna activera vid det nedtrycket XD

	while (readisInited == 0) { //V�ntar tills readis �r ig�ng
		Sleep(100);
	}


	//std::cout << "Hotkeys:\n";
	//std::cout << "Toggle key: ";
	toggleKey = whichKeyIsPressed(); //denna kommer k�ras snabbt, kan d�rav vara vettigare att s�tta variablen till �nskat vkey v�rde
	//std::cout << toggleKey << "\n";
	//std::cout << "\n";

	//std::cout << "The hack is now active!\n\n";

	me.oldPunch.x = 0;
	me.oldPunch.y = 0;

	while (!GetAsyncKeyState(VK_END)) { //Om END p� tangentbordet klickas avslutas denna tr�d

		/*if (GetAsyncKeyState(VK_INSERT)) {
			GetCursorPos(&cursor);
		}*/

		if (GetAsyncKeyState(toggleKey) & 0x8000) {
			activated = !activated;
			//std::cout << "RCS is now ";
			if (activated) {
				RCSstate = 1;
				//std::cout << "enabled";
			}
			else {
				RCSstate = 0;
				//std::cout << "disabled";
			}
			//std::cout << "\n";
			Sleep(200); 
		}



		me.m_vecPunch = mem.Read<Vector>(dwPlayer + Offsets()->dwVecPunch);
		me.m_vecPunch.x *= 2.0f;
		me.m_vecPunch.y *= 2.0f;
		deltaX = toPixels(me.m_vecPunch.x); //H�r blire data loss ev. varningar i vald IDE pga skit konvertering
		deltaY = toPixels(me.m_vecPunch.y);

		me.m_iShotsFired = mem.Read<int>(dwPlayer + Offsets()->dwIShotsFired); //Har koll p� antal skott

		if (me.m_iShotsFired > 0 && activated) {
			Vector currentAngles = mem.Read<Vector>(dwEngine + Offsets()->dwSetViewAngle);

			Vector modifier = me.m_vecPunch;
			modifier.x -= me.oldPunch.x;
			modifier.y -= me.oldPunch.y;

			currentAngles.x -= modifier.x;
			currentAngles.y -= modifier.y;

			if (currentAngles.x < -180.0f) currentAngles.x += 360.0f;
			if (currentAngles.x > 180.0f) currentAngles.x -= 360.0f;

			if (currentAngles.y < -180.0f) currentAngles.y += 360.0f;
			if (currentAngles.y > 180.0f) currentAngles.y -= 360.0f;

			currentAngles.z = 0; //OM DENNA �R != 0 BLIRE VAC-BANN! INSTA- HOLYSHIET!

			if (currentAngles.x > 89.0f && currentAngles.x <= 180.0f) {
				currentAngles.x = 89.0f;
			}

			if (currentAngles.x < -89.0f) {
				currentAngles.x = -89.0f;
			}


			//!!!! toPixels(currentAngles.x) returnerar hur m�nga pixlar under startpositionen den skall vara!!!
			//SetCursorPos(Width/2 - toPixels(currentAngles.x), Height/2 - toPixels(currentAngles.y));
			mem.Write<Vector>(dwEngine + Offsets()->dwSetViewAngle, currentAngles);
			//std::cout << currentAngles.x << "\n";	//Kolla ner�t 89 / upp�t -89	//H�gre �r ILLEGAL ANGLES = VAC-BANN
			//std::cout << currentAngles.y << "\n";	//Norr 90, V�st +-180, �st 0, syder -90	//H�gre �r ILLEGAL ANGLES = VAC-BANN
			//currentAngles.z ska alltid vara 0 ev. �ndras vid p�verkan av explosioner?
			//std::cout << me.m_iShotsFired << "\n";	//Skott
		}

		me.oldPunch = me.m_vecPunch;

		while (synced == 0) {
			Sleep(1);
		}
	}
}


void readis() {


	while (!mem.Attach("csgo.exe")) { //Waits for CS:GO to start and attaches
		Sleep(100);
	}
	Offsets()->getOffsets(&mem);

	modClient = mem.GetModule("client.dll");
	modEngine = mem.GetModule("engine.dll");


	readisInited = 1;
	while (!GetAsyncKeyState(VK_END)) {
		dwEngine = mem.Read<DWORD>(modEngine.dwBase + Offsets()->dwEnginePointer);
		currSens = mem.Read<float>(modClient.dwBase + Offsets()->dwSensitivity);


		for (int i = 0; i < 64; i++) {
			DWORD currentEntity = mem.Read<int>(modClient.dwBase + i * Offsets()->m_EntityLoopDistance + Offsets()->dwEntityList);
			//DWORD currnetEntityRadar = mem.Read<int>(modClient.dwBase + i * Offsets()->m_EntityLoopDistance + Offsets()->m_dwRadarBase);
			nr[i].ID = mem.Read<int>(currentEntity + Offsets()->m_dwIndex);
			nr[i].Team = mem.Read<int>(currentEntity + Offsets()->m_iTeamNum);
			nr[i].Health = mem.Read<int>(currentEntity + Offsets()->m_iHealth);
			nr[i].Dormant = mem.Read<bool>(currentEntity + Offsets()->m_bDormant);
		}


		synced = 0;
		while (synced == 0) {
			Sleep(1);
		}
	}
}

void syncer()
{
	int temp = 0;
	while (readisInited == 0) { //V�ntar tills readis �r ig�ng
		Sleep(1);
	}
	while (!GetAsyncKeyState(VK_END)) {
		dwPlayer = mem.Read<DWORD>(modClient.dwBase + Offsets()->dwLocalPlayer); //Get our player's information
		tickcount = mem.Read<int>(dwPlayer + Offsets()->m_nTickBase); //Get current tickcount
		if (temp == tickcount) {

		}
		else {
			synced = 1;
			temp = tickcount;
		}
		std::this_thread::sleep_for(std::chrono::microseconds(250));
	}
}

void bhopper()
{
	std::default_random_engine generator;
	std::lognormal_distribution<double> distribution(0.0, 1.0);
	int tempDelay = 0;


	while (readisInited == 0) { //V�ntar tills readis �r ig�ng
		Sleep(100);
	}
	while (!GetAsyncKeyState(VK_END)) {
		
		Flags = mem.Read<int>(dwPlayer + Offsets()->m_fFlags); //F�r l�ngsam om den ligger i readis pga shit
		if ((GetAsyncKeyState(VK_SPACE) & 0x8000) && ((Flags & 0x1) == 1)) {
			Sleep(tempDelay);
			mem.Write<int>((modClient.dwBase + Offsets()->dwForceJump), 1);
			tempDelay = floor(distribution(generator));
			std::this_thread::sleep_for(std::chrono::milliseconds(rand()%3+1));
			mem.Write<int>((modClient.dwBase + Offsets()->dwForceJump), 0);
		}
		std::this_thread::sleep_for(std::chrono::microseconds(250));
	}
}