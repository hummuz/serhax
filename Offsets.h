#ifndef OFFSETS_H
#define OFFSETS_H

#include "PMemory.h"

#include <string>
#include <sstream>
//#include "Decrypt.h"

//[junk_enable /]
//[junk_enable_declares /]

class COffsets {
public:
	//Team-names:		None = 0, Spectator = 1, Terrorists = 2, CounterTerrorists = 3
	// LifeStates:		Alive = 0, KillCam = 1, Dead = 2
	// https://github.com/BigMo/Zat-s-External-CSGO-Multihack-v3/blob/master/CSGO/CSGOClassID.cs
	// https://github.com/AlvyPiper/MoreGreenTea/blob/master/csgosdk/game/client/



	//DWORD m_dwRadarBase = 0x04A9166C;

	// Dessa har offset-scanning:
	DWORD dwLocalPlayer;	// Pekar p� b�rjan av en struct med spelaren som �r fokuserads information
	DWORD dwEnginePointer;	// aka ClientState
	DWORD dwSetViewAngle;	// Pekar p� addressen d�r spelarens roll pitch yaw f�s och s�tts
	DWORD dwForceJump;		// Pekar p� addressen som s�tts till 1 f�r +jump och 0 f�r -jump
	DWORD dwSensitivity;	// Addressen f�r en float med spelarens mouse sens (kan s�ttas)
	DWORD dwEntityList;		// Pekar p� b�rjan av MAX_PLAYERS antal structer med info om respektive spelare
	// Dessa kan beh�va �ndras vid csgo update:
	DWORD dwVecPunch = 0x3018;
	DWORD dwIShotsFired = 0xA2A0;
	DWORD m_nTickBase = 0x340C;
	// Dessa beh�vs s� gott som aldrig �ndras
	DWORD m_fFlags = 0x100;
	DWORD m_iTeamNum = 0xF0;
	DWORD m_iHealth = 0xFC;
	DWORD m_EntityLoopDistance = 0x10;
	DWORD m_RadarLoopDistance = 0x1E0;
	DWORD m_bDormant = 0xE9;
	DWORD m_dwIndex = 0x64;


	std::string toHex(DWORD offset);

	void getOffsets(PMemory* m);

private:
	PMemory* mem;
	PModule modClient, modEngine;

	void updateLocalPlayer();
	void updateEnginePointer();
	void updateForceJump();
	void updateSensitivity();
	void updateEntityList();

};

inline COffsets* Offsets() {
	static COffsets instance;
	return &instance;
}

#endif